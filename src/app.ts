let implicitCoupon = 'pizza25';
let explicitCoupon: string;
explicitCoupon = 'pizza25';




let selectedTopping: string = 'pepperoni';

function selectTopping(topping: string): void {
  selectedTopping = topping;
}

selectTopping('bacon');

console.log(selectedTopping);


function orderError(error: string): never {
  throw new Error(error);
  //never going to return a value!
}

orderError('Something went wrong');